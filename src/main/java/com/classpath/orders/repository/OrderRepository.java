package com.classpath.orders.repository;

import com.classpath.orders.model.Order;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class OrderRepository {

    //specify the data types
    private final static Map<Long, Order> orderMap = new HashMap<>();

    public Order save(Order order) {
        long orderId = Math.round(Math.random() * 25234324234L);
        order.setOrderId(orderId);
        orderMap.put(orderId, order);
        return order;
    }

    public Set<Order> fetchAll() {
        return orderMap
                    .values()
                     .stream()
                     .collect(Collectors.toSet());
    }

    public Optional<Order> findOrderByOrderId(long orderId) {
        return Optional.ofNullable(orderMap.get(orderId));
    }

    public void deleteOrderById(long orderId) {
        orderMap.remove(orderId);
    }
}