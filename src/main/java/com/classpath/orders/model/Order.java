package com.classpath.orders.model;

import lombok.*;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Order {

    private long orderId;

    private String customerName;

    private double price;

    private LocalDate date;

    private String emailAddres;

}