package com.classpath.orders.service;

import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;

import java.util.Set;

public class OrderService {

    private OrderRepository orderRepository = new OrderRepository();

    public Order saveOrder(Order order){
        //validate the data
        //process the data
        //persist the data to the data store
        return orderRepository.save(order);
    }

    public Set<Order> fetchAll(){
        return orderRepository.fetchAll();
    }

    public Order fetchOrderById(long orderId){
        return orderRepository
                .findOrderByOrderId(orderId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid order id passed"));
    }

    public Order updateOrder(Order order){
        return order;
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteOrderById(orderId);
    }
}